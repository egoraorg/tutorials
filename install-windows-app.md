# Tutorial for installing Riot on Windows 10
## 1. Go to the Riot downloads webpage
Go to https://riot.im/download/desktop/



## 2. Download the Riot client
### 32 bit:
 Currently windows 32 bit is not supported
### 64 bit
Click the Windows 10 or later (64bit) link 

![Download Win](img/install-windows-app/win-download.png "download Win")

## 3. Install the Riot client
When the download is done, click the Riot Setup file

![Download progress](img/install-windows-app/win-install.png "Download progress")

A small window will open up


![Drag](img/install-windows-app/win-save-as.png "Drag")

Click save file, you might have to click twice. The Riot client will auto install and open up


