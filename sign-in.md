# Tutorial for signing in on Egora.org's Matrix Server
## 1. Open your app.
For the purpose of the tutorial we will assume you are using the web client, but desktop is the same, and mobile apps work the same way but look a bit different.

![Open App](img/create-user/createUser0.png "Open App")

## 2. Click Sign In.

![Click Sign In](img/create-user/createUser1.png "Click Sign In")

## 3. Click Change.
This is important as otherwise you will be logging in on Matrix.org's server rather than Egora.org, which will fail as you hopefully followed the create account guide correctly and registered your user on Egora.org's server.

![Click Change](img/sign-in/signIn1.png "Click Change")

## 4. Click Other and insert the link to the server: https://matrix.egora.org, then click Next.
Remember to include the https://

![Click Other and Insert Server Link](img/create-user/createUser2.png "Click Other and Insert Server Link")

## 5. Enter your Username and Password.

![Enter your Username and Password](img/sign-in/signIn1.png "Enter your Username and Password")

## 6. (If prompted) Enter your recovery passphrase to enable encryption.

### If you have not already configured encryption when you created your account, it will ask you to enter a new recovery passphrase.

![Enter your recovery passphrase to enable encryption.](img/sign-in/signIn2.png "EEnter your recovery passphrase to enable encryption.")

### If you have already entered a recovery passphrase and this is your first time logging in from a new app or device, it will ask you to verify, if you have no other device or app connected, click the link to enter your recovery passphrase.

![Enter your recovery passphrase to enable encryption.](img/sign-in/signIn3.png "EEnter your recovery passphrase to enable encryption.")

## 7. Welcome, you are now logged in.
To start out you should join some rooms by clicking the Explore button in the top left corner.