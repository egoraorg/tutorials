# Structure

## Tutorial files
Tutorial files should be named after the user action, eg. "install-linux-app", with hyphens between each word, all letters should be small.

## Images
Images for a tutorial file should be saved in a folder in the /img folder with the same name as the tutorial file.