# Tutorial for installing Riot on macOS
## 1. Go to the Riot downloads webpage
Go to https://riot.im/download/desktop/



## 2. Download the Riot client
Click the macOS. Riot-"version".dmg should start downloading

![Download Mac](img/install-macos-app/mac-download.png "download Mac")

## 3. Install the Riot client
When the download is done, click the Riot .dmg file

![Download progress](img/install-macos-app/mac-dowload-progress.png "Download progress")

A small window will open up while the dmg file is unpacking


![Drag](img/install-macos-app/mac-drag.png "Drag")

When it is done unpacking, drag the Riot icon into the Applications folder. Now you should be all set


